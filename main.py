#!/usr/bin/env python
import sys
import os


def main():
	from PIL import Image

	FILENAME = sys.argv[1] #"./titulo_achg.pdf"
	IMAGES_PATH = sys.argv[2]
	images_array = []
	output_images = []
	rgb = []
	list = []

	for file in os.listdir(IMAGES_PATH):
		if '.jpg' in file or '.png' in file:
			images_array.append(file)

	for index, image in enumerate(images_array, start=0):
		output_images.append(Image.open(image))
		image = output_images[index]
		rgb.append(Image.new("RGB", image.size, (255,255,255)))
		rgb[index].paste(image)
		if index > 0:
			list.append(rgb[index])

	rgb[0].save(FILENAME, "PDF", resolution=100.0, save_all=True, append_images=list)


if __name__ == '__main__':
	main()
